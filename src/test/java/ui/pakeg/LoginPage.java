package ui.pakeg;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {
    String username = "Admin";
    String password = "admin123";
    SelenideElement usernameXpath = $x("//input [@name='username']");
    SelenideElement passwordXpath = $x("//input [@name='password']");
    SelenideElement loginButton = $x("//button [@type='submit']");
    SelenideElement user = $x("//i [@class='oxd-icon bi-caret-down-fill oxd-userdropdown-icon']");
    SelenideElement logout = $x("//a [text()='Logout']");

    public void open() {
        Selenide.open("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
    }

    public void logIn() {
        usernameXpath.setValue(username);  //ввели пользователя
        passwordXpath.setValue(password);  //ввели пароль
        loginButton.click();  //нажали войти
    }

    public void logOut() {
        user.click();
        logout.click();
    }
}