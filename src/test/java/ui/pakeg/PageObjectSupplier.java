package ui.pakeg;

public interface PageObjectSupplier {
    default LoginPage loginPage() {
        return new LoginPage();
    }

    default HomePage homePage() {
        return new HomePage();
    }

    default MyInfo myInfo() {
        return new MyInfo();
    }
}