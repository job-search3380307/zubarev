package ui.pakeg;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class MyInfo {
    SelenideElement personalDetails = $x("//a [text()='Personal Details']");
    SelenideElement contactDetails = $x("//a [text()='Contact Details']");
    SelenideElement emergencyContacts = $x("//a [text()='Emergency Contacts']");

    public void myInfoMenu() {
        personalDetails.shouldBe(text("Personal Details"));
        contactDetails.shouldBe(text("Contact Details"));
        emergencyContacts.shouldBe(text("Emergency Contacts"));
    }
}

