package ui.pakeg;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class HomePage {
    SelenideElement myInfo = $x("//span [text()='My Info']");
    SelenideElement search = $x("//input [@placeholder='Search']");
    SelenideElement menuAdmin = $x("//span [text()='Admin']");
    SelenideElement menuDashboard = $x("//span [text()='Dashboard']");
    SelenideElement menuDirectory = $x("//span [text()='Directory']");

    public void myInfoClick() {
        myInfo.click();
    }

    public void search(String searchValue) {
        search.setValue(searchValue);
    }

    public void searchTest() {
        menuAdmin.shouldBe(text("Admin"));
        menuDashboard.shouldBe(text("Dashboard"));
        menuDirectory.shouldBe(text("Directory"));

    }
}