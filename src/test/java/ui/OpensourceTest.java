package ui;


import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ui.pakeg.PageObjectSupplier;

public class OpensourceTest implements PageObjectSupplier {

    @BeforeTest
    public void logIn() {
        loginPage().open();
        loginPage().logIn();
    }

    @AfterTest
    public void logOut() {
        loginPage().logOut();
    }

    @Test
    public void myInfoTest() {
        homePage().myInfoClick();
        myInfo().myInfoMenu();
    }

    @Test
    public void searchTest() {
        homePage().search("d");
        homePage().searchTest();
    }
}