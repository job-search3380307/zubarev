package api;

import models.User;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.Test;

import static api.BaseTest.requestSpec;
import static api.BaseTest.responseSpec;

public class UserTest {
    User user = new User(1230, "Ivanov1929", "Ivanov", "Ivan", "ivanov@gmail.com", "!@#$%^^", "326548", 1456987);
    User userNew = new User(1230, "Ivanov1929", "Ivanov", "Ivan", "ivanov@gmail.com", "!@#$%^^", "326548", 145698712);

    @Test(priority = 1, description = "Создаем пользователя")
    public void newUserTest() {
        RestAssured
                .given()
                .spec(requestSpec())
                .body(user)
                .when()
                .post("/user")
                .then()
                .spec(responseSpec())
                .extract().body();
    }

    @Test(priority = 2, description = "Проверяем что пользователь создался корректно")
    public void checkCreatedUserTest() {
        User userNew = RestAssured
                .given()
                .spec(requestSpec())
                .when()
                .get("/user/Ivanov1929")
                .then()
                .spec(responseSpec())
                .extract().body().as(User.class);

        Assert.assertEquals(userNew, user, "пользователь создался не корректно");
    }

    @Test(priority = 3, description = "Вход созданным пользователем")
    public void LoginTest() {
        RestAssured
                .given()
                .spec(requestSpec())
                .queryParam("username", "Ivanov1929")
                .queryParam("password", "!%40%23%24%25%5E%5E")
                .when()
                .get("/user/login")
                .then()
                .spec(responseSpec())
                .extract().body();
    }

    @Test(priority = 4, description = "Обновляем данные у пользователя")
    public void userUpdateTest() {
        RestAssured
                .given()
                .spec(requestSpec())
                .body(userNew)
                .when()
                .put("/user/Ivanov1929")
                .then()
                .spec(responseSpec())
                .extract().body();
    }

    @Test(priority = 5, description = "Проверяем что данные у пользователя обновились")
    public void checkUpdateUserTest() {
        User userNew = RestAssured
                .given()
                .spec(requestSpec())
                .when()
                .get("/user/Ivanov1929")
                .then()
                .spec(responseSpec())
                .extract().body().as(User.class);

        Assert.assertNotEquals(userNew, user, "пользователь обновился не корректно");
    }

    @Test(priority = 6, description = "Удаляем пользователя")
    public void deleteUserTest() {
        RestAssured
                .given()
                .spec(requestSpec())
                .when()
                .delete("/user/Ivanov1929")
                .then()
                .spec(responseSpec())
                .extract().body();
    }
}