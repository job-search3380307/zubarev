package api;

public enum Header {
    CONTENT_TYPE("Content-Type","application/json");

    private String headerName;
    private String headerValue;

    Header(String headerName, String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    public String headerName(){
        return headerName;
    }
    public String headerValue(){
        return headerValue;
    }
}