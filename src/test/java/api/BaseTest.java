package api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static api.Header.*;

public class BaseTest {
    protected static RequestSpecification requestSpec() {
        RequestSpecBuilder rqb = new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri("https://petstore.swagger.io/v2/")
                .addHeader(CONTENT_TYPE.headerName(), CONTENT_TYPE.headerValue());

        return rqb.build();
    }

    protected static ResponseSpecification responseSpec() {

        return new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .expectStatusCode(200)
                .expectHeader(CONTENT_TYPE.headerName(), CONTENT_TYPE.headerValue())
                .expectContentType(CONTENT_TYPE.headerValue())
                .build();
    }
}